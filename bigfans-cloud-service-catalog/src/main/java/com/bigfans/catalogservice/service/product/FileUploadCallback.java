package com.bigfans.catalogservice.service.product;

import java.io.File;

public interface FileUploadCallback {
    void onComplete(String filepath, File file) throws Exception;
}
