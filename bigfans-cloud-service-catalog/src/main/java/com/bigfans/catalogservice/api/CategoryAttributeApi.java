package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.model.AttributeOption;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.catalogservice.service.attribute.AttributeOptionService;
import com.bigfans.catalogservice.service.attribute.AttributeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoryAttributeApi extends BaseController {

    @Autowired
    private AttributeOptionService attributeOptionService;
    @Autowired
    private AttributeValueService attributeValueService;

    @GetMapping(value = "/com/bigfans/categoryservice/{id}/attributeOption")
    public RestResponse listAttributeOptions(@PathVariable(value = "id") String id) throws Exception {
        List<AttributeOption> attributeOptions = attributeOptionService.listByCategory(id);
        RestResponse resp = RestResponse.ok();
        return resp;
    }

}
