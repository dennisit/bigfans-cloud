package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.service.productgroup.ProductGroupService;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductGroupApi {

	@Autowired
	private ProductGroupService productGroupService;

	@GetMapping(value = "/productGroup/detail")
	public RestResponse detail(
			@PathVariable(value = "pgId" , required = false) String pgId,
			@RequestParam(value = "prodId" , required = false) String prodId
	) throws Exception {
		String description = productGroupService.getDescription(pgId, prodId);
		return RestResponse.ok(description);
	}
}
