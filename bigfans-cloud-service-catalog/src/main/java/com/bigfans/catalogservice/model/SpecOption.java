package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.SpecOptionEntity;
import lombok.Data;

import java.util.List;

/**
 * 
 * @Description:商品规格项
 * @author lichong 2015年5月31日上午9:19:03
 *
 */
@Data
public class SpecOption extends SpecOptionEntity {

	private static final long serialVersionUID = 8617307772698864089L;

	private String pid;
	private String pgId;
	
	/* 收集页面上的值 */
	private List<String> values;
	/* 传数据回页面 */
	private List<SpecValue> specValues;

	@Override
	public String toString() {
		return "SpecOption [pid=" + pid + ", pgId=" + pgId + ", specValues=" + specValues + ", name=" + name
				+ ", categoryId=" + categoryId + ", code=" + code + ", inputType=" + inputType + ", orderNum="
				+ orderNum + ", id=" + id + ", createDate=" + createDate + ", updateDate=" + updateDate + ", deleted="
				+ deleted + "]";
	}

}
