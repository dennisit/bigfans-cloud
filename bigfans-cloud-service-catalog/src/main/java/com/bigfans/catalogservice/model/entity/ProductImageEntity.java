package com.bigfans.catalogservice.model.entity;

import com.bigfans.catalogservice.model.ProductImage;
import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Description:图片
 * @author lichong 2015年1月5日上午8:26:42
 *
 */
@Data
@Table(name = "Product_Image")
public class ProductImageEntity extends AbstractModel implements Cloneable {

	private static final long serialVersionUID = 8817400056171607998L;

	public static final String TYPE_LARGE = "L";
	public static final String TYPE_MIDDLE = "M";
	public static final String TYPE_SMALL = "S";
	public static final String TYPE_THUMB = "TH";
	public static final int SIZE_WEIDTH_L = 800;
	public static final int SIZE_WEIDTH_M = 300;
	public static final int SIZE_WEIDTH_S = 80;
	public static final int SIZE_HEIGHT_L = 800;
	public static final int SIZE_HEIGHT_M = 300;
	public static final int SIZE_HEIGHT_S = 80;

	public static final String EXTENSION = "jpg";

	public String getModule() {
		return "ProductImage";
	}

	@Column(name = "prod_id")
	protected String prodId;
	@Column(name = "pg_id")
	protected String pgId;
	@Column(name = "type")
	protected String type;
	@Column(name = "path")
	protected String path;
	@Column(name = "order_num")
	protected Integer orderNum;
	@Column(name = "storage_type")
	protected String storageType;
	@Column(name = "server")
	protected String server;

	@Override
	public ProductImage clone() throws CloneNotSupportedException {
		return (ProductImage) super.clone();
	}
}
