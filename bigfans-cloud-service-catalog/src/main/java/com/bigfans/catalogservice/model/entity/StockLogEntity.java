package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

@Data
@Table(name = "Stock_Log")
public class StockLogEntity extends AbstractModel {

    public static final String DIR_IN = "in";
    public static final String DIR_OUT = "out";

    public static final String REASON_ORDERCREATE = "ordercreate";
    public static final String REASON_ORDERCANCEL = "ordercancel";
    public static final String REASON_ORDERREVERT = "orderrevert";

    @Column(name = "user_id")
    private String userId;
    @Column(name = "order_id")
    private String orderId;
    @Column(name = "prod_id")
    private String prodId;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "direction")
    private String direction;
    @Column(name = "reason")
    private String reason;

    @Override
    public String getModule() {
        return "StockLog";
    }
}
