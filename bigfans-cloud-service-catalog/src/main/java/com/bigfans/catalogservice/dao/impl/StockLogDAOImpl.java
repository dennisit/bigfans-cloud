package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.StockLogDAO;
import com.bigfans.catalogservice.model.StockLog;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Stock log
 * 
 * @Description:
 * @author lichong
 * @date Mar 7, 2016 4:04:31 PM
 *
 */
@Repository(StockLogDAOImpl.BEAN_NAME)
public class StockLogDAOImpl extends MybatisDAOImpl<StockLog> implements StockLogDAO {

	public static final String BEAN_NAME = "stockLogDAO";

	@Override
	public List<StockLog> listByOrder(String orderId) {
		ParameterMap params = new ParameterMap();
		params.put("orderId", orderId);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<StockLog> listByOrder(String orderId, String direction) {
		ParameterMap params = new ParameterMap();
		params.put("orderId", orderId);
		params.put("direction", direction);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public Integer countByOrder(String orderId) {
		ParameterMap params = new ParameterMap();
		params.put("id", orderId);
		return getSqlSession().selectOne(className + ".count", params);
	}
}
