package com.bigfans.searchservice;

import com.bigfans.searchservice.service.CategoryIndexService;
import com.bigfans.searchservice.service.TagIndexService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchServiceApp.class)
public class CategoryMappingTest {

    @Autowired
    private CategoryIndexService categoryIndexService;

    @Test
    public void testCreate(){
        try {
            categoryIndexService.create();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
