package com.bigfans.searchservice.schema.mapping;

import com.bigfans.framework.es.BaseMapping;
import com.bigfans.framework.es.schema.IndexSettingsAnalyzer_IK;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;

public class ProductMapping extends BaseMapping {

	public static final String INDEX = "product";
	public static final String TYPE = "Product";
	public static final String ALIAS = "product_candidate";
	public static final String FIELD_ID = "id";
	public static final String FIELD_RAW = "raw";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_NAME_IK = "ik";
	public static final String FIELD_FEATURES = "features";
	public static final String FIELD_BRAND = "brand";
	public static final String FIELD_BRAND_ID = "brandId";
	public static final String FIELD_PRICE = "price";
	public static final String FIELD_CATEGORY = "category";
	public static final String FIELD_CATEGORY_ID = "categoryId";
	public static final String FIELD_ORIGIN = "origin";
	public static final String FIELD_IMAGE_PATH = "imagePath";
	
	public static final String FIELD_SPECS = "specs";
	public static final String FIELD_SPEC_OPTID = "option_id";
	public static final String FIELD_SPEC_OPTNAME = "option_name";
	public static final String FIELD_SPEC_VALID = "value_id";
	public static final String FIELD_SPEC_VALNAME = "value_name";
	
	public static final String FIELD_ATTRIBUTES = "attributes";
	public static final String FIELD_ATTRIBUTE_OPTID = "option_id";
	public static final String FIELD_ATTRIBUTE_OPTNAME = "option_name";
	public static final String FIELD_ATTRIBUTE_VALID = "value_id";
	public static final String FIELD_ATTRIBUTE_VALNAME = "value_name";
	
	public static final String FIELD_TAGS = "tags";
	public static final String FIELD_TAG_ID = "id";
	public static final String FIELD_TAG_NAME = "name";
	
	@Override
	public String getType() {
		return TYPE;
	}
	
	@Override
	public String getIndex() {
		return INDEX;
	}
	
	public Object getMapping() {
		XContentBuilder schemaBuilder = null;
		try {
			schemaBuilder = XContentFactory.jsonBuilder()
					.startObject()
						.startObject(TYPE)
							.startObject("properties")
								.startObject(FIELD_ID)
									.field("type", "keyword")
									.field("store", true)
								.endObject()
								.startObject(FIELD_NAME)  
						            .field("type", "text")
									.field("index", true)
								    .field("analyzer", IndexSettingsAnalyzer_IK.NAME)
						            .startObject("fields")
					            		.startObject(FIELD_RAW)
					            			.field("type", "keyword")  
					            		.endObject()
						            .endObject()
						        .endObject()
						        .startObject(FIELD_SPECS)  
						            .field("type", "nested")  
						            .startObject("properties")
						            	.startObject(FIELD_SPEC_OPTID)
						            		.field("type", "keyword")
						            	.endObject()
						            	.startObject(FIELD_SPEC_OPTNAME)
						            		.field("type", "keyword")
						            	.endObject()
						            	.startObject(FIELD_SPEC_VALID)
						            		.field("type", "keyword")
						            	.endObject()
						            	.startObject(FIELD_SPEC_VALNAME)
						            		.field("type", "keyword")
						            	.endObject()
						            .endObject()
					            .endObject()
						        .startObject(FIELD_ATTRIBUTES)  
						            .field("type", "nested")  
						            .startObject("properties")
						            	.startObject(FIELD_ATTRIBUTE_OPTID)
						            		.field("type", "keyword")
						            	.endObject()
						            	.startObject(FIELD_ATTRIBUTE_OPTNAME)
						            		.field("type", "keyword")
						            	.endObject()
						            	.startObject(FIELD_ATTRIBUTE_VALID)
						            		.field("type", "keyword")
						            	.endObject()
						            	.startObject(FIELD_ATTRIBUTE_VALNAME)
						            		.field("type", "keyword")
						            	.endObject()
						            .endObject()
					            .endObject()
					            .startObject(FIELD_TAGS)  
						            .field("type", "nested")  
						            .startObject("properties")
						            	.startObject("id")
						            		.field("type", "keyword")
						            	.endObject()
						            	.startObject("name")
						            		.field("type", "keyword")
						            	.endObject()
						            .endObject()
					            .endObject()
					            .startObject(FIELD_FEATURES)  
						            .field("type", "text")
						            .field("analyzer", "whitespace")
						        .endObject()
						        .startObject(FIELD_PRICE)  
						            .field("type", "double")  
						        .endObject()
						        .startObject(FIELD_IMAGE_PATH)  
						            .field("type", "keyword")  
						        .endObject()
						        .startObject(FIELD_CATEGORY_ID)  
						            .field("type", "keyword")  
						        .endObject()
						        .startObject(FIELD_CATEGORY)  
						            .field("type", "keyword")  
						        .endObject()
						        .startObject(FIELD_ORIGIN)  
						            .field("type", "keyword")  
						        .endObject()
						        .startObject(FIELD_BRAND)  
						            .field("type", "keyword")  
						        .endObject()
						        .startObject(FIELD_BRAND_ID)  
						            .field("type", "keyword")  
						        .endObject()
						     .endObject()
						 .endObject()
					.endObject();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return schemaBuilder;
	}

}
