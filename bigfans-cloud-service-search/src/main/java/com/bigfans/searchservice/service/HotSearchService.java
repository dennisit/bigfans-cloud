package com.bigfans.searchservice.service;

import java.util.List;

public interface HotSearchService {

    List<String> hotSearch();

    void put(String keyword);

}
