package com.bigfans.searchservice.service.impl;

import com.bigfans.framework.es.ElasticTemplate;
import com.bigfans.framework.es.request.MoreLikeThisSearchCriteria;
import com.bigfans.framework.es.request.SearchResult;
import com.bigfans.searchservice.document.convertor.ProductDocumentConverter;
import com.bigfans.searchservice.model.Product;
import com.bigfans.searchservice.schema.mapping.ProductMapping;
import com.bigfans.searchservice.service.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lichong
 * @create 2018-03-18 上午11:51
 **/
@Service
public class RecommendServiceImpl implements RecommendService{

    public static final String BEAN_NAME = "recommendService";

    @Autowired
    private ElasticTemplate elasticTemplate;

    public SearchResult<Product> moreLikeThis(String prodId , Integer limit){
        MoreLikeThisSearchCriteria mtsc = new MoreLikeThisSearchCriteria();
        mtsc.setIndex(ProductMapping.INDEX);
        mtsc.setType(ProductMapping.TYPE);
        mtsc.setSourceDocId(prodId);
        mtsc.setSize(limit);
        SearchResult<Product> productSearchResult = elasticTemplate.moreLikeThisDocument(mtsc, new ProductDocumentConverter());
        return productSearchResult;
    }

}
