package com.bigfans.searchservice.model.vo;

import com.bigfans.framework.utils.CollectionUtils;
import com.bigfans.framework.utils.StringHelper;
import com.bigfans.searchservice.api.request.HttpParams;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;


/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2016年3月28日 上午8:03:31 
 * @version V1.0
 */
@Data
public class FilterUrlBuilder {

	private StringBuilder url;
	private String categoryParam;
	private String brandParam;
	private Map<String , String> attrFilters;
	
	public void url(StringBuilder url){
		this.url = new StringBuilder(url);
	}
	
	public void cat(String categoryParam){
		this.categoryParam = categoryParam;
	}
	
	public void brand(String brandParam){
		this.brandParam = brandParam;
	}
	
	public void attr(String key , String value){
		if(attrFilters == null){
			attrFilters = new HashMap<>();
		}
		this.attrFilters.put(key, value);
	}
	
	public void attr(Map<String , String> filterMap){
		if(attrFilters == null){
			attrFilters = new HashMap<>();
		}
		this.attrFilters.putAll(filterMap);
	}
	
	public String build(){
		if(StringHelper.isNotEmpty(categoryParam)){
			url.append("&").append(HttpParams.SearchFilter.CATEGORY).append("=").append(categoryParam);
		}
		if(StringHelper.isNotEmpty(brandParam)){
			url.append("&").append(HttpParams.SearchFilter.BRAND).append("=").append(brandParam);
		}
		if(CollectionUtils.isNotEmpty(attrFilters)){
			url.append("&").append(HttpParams.SearchFilter.FILTER).append("=");
			for (Map.Entry<String, String> entry : attrFilters.entrySet()) {
				url.append(entry.getKey()).append("_").append(entry.getValue()).append(",");
			}
		}
		return url.toString();
	}
}
