package com.bigfans.sleuth.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import zipkin.server.EnableZipkinServer;

/**
 * @author lichong
 * @create 2018-02-04 下午6:23
 **/
@SpringBootApplication
@EnableZipkinServer
public class ZipkinServerApp {

    public static void main(String[] args) throws Exception{
        SpringApplication.run(ZipkinServerApp.class , args);
    }

}
