package com.bigfans.orderservice.exception;

import com.bigfans.framework.exception.ServiceRuntimeException;

public class OrderCancelException extends ServiceRuntimeException {

    public OrderCancelException(String message) {
        super(message);
    }

    public OrderCancelException(Integer errorCode, String message) {
        super(errorCode, message);
    }
}
