package com.bigfans.orderservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.orderservice.model.OrderItem;

import java.util.List;

/**
 * @author lichong
 * 2015年1月17日下午8:02:19
 * @Description:订单条目DAO
 */
public interface OrderItemDAO extends BaseDAO<OrderItem> {

    List<OrderItem> listByUserOrder(String userId, String orderId);

    List<OrderItem> listByOrder(String orderId);

    List<OrderItem> listUnCommentedByOrder(String userId, String orderId);

    List<OrderItem> listByUser(String userId, Long start, Long pagesize);

    List<OrderItem> listByUser(String userId, Integer status, Long start, Long pagesize);

}
