package com.bigfans.cartservice.service.impl;

import com.bigfans.cartservice.dao.ProductSpecDAO;
import com.bigfans.cartservice.model.ProductSpec;
import com.bigfans.cartservice.service.ProductSpecService;
import com.bigfans.framework.dao.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lichong
 * @create 2018-03-24 上午9:39
 **/
@Service(ProductSpecServiceImpl.BEAN_NAME)
public class ProductSpecServiceImpl extends BaseServiceImpl<ProductSpec> implements ProductSpecService {

    public static final String BEAN_NAME = "productSpecService";

    private ProductSpecDAO productSpecDAO;

    @Autowired
    public ProductSpecServiceImpl(ProductSpecDAO productSpecDAO) {
        super(productSpecDAO);
        this.productSpecDAO = productSpecDAO;
    }

    public List<ProductSpec> listProductSpecs(String prodId) throws Exception{
        return productSpecDAO.listByProdId(prodId);
    }
}
