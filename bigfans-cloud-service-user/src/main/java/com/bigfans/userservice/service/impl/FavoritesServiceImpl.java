package com.bigfans.userservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.framework.model.PageBean;
import com.bigfans.framework.model.PageContext;
import com.bigfans.userservice.dao.FavoritesDAO;
import com.bigfans.userservice.model.Favorites;
import com.bigfans.userservice.service.FavoritesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 
 * @Description: 
 * @author lichong
 * @date Mar 10, 2016 3:27:47 PM 
 *
 */
@Service(FavoritesServiceImpl.BEAN_NAME)
public class FavoritesServiceImpl extends BaseServiceImpl<Favorites> implements FavoritesService {

	public static final String BEAN_NAME = "favoritesService";
	
	private FavoritesDAO favoritesDAO;
	
	@Autowired
	public FavoritesServiceImpl(FavoritesDAO favoritesDAO) {
		super(favoritesDAO);
		this.favoritesDAO = favoritesDAO;
	}
	
	@Override
	@Transactional(readOnly = true)
	public PageBean<Favorites> pageByUser(String userId, Long start , Long pagesize) throws Exception {
		List<Favorites> myfavorates = favoritesDAO.listByUser(userId, start, pagesize);
		PageBean<Favorites> pageBean = new PageBean<Favorites>(myfavorates , PageContext.getDataCount());
		return pageBean;
	}
	
	@Override
	@Transactional
	public void create(Favorites e) throws Exception {
		Favorites myFavor = this.getByUser(e.getUserId(), e.getProductId());
		if(myFavor != null){
			throw new Exception("已经添加过该商品");
		}
		super.create(e);
	}

	@Override
	@Transactional(readOnly = true)
	public Favorites getByUser(String userId, String prodId) {
		return favoritesDAO.getByUser(userId, prodId);
	}

}
