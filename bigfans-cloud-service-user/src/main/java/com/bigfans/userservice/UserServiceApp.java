package com.bigfans.userservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jms.JmsAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;


/**
 * 
 * bigfans服务
 *
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude={
		JmsAutoConfiguration.class , 
		ElasticsearchAutoConfiguration.class , 
		RedisAutoConfiguration.class,
		DataSourceAutoConfiguration.class
		})
public class UserServiceApp
{
    public static void main( String[] args )
    {
    	ConfigurableApplicationContext applicationContext = SpringApplication.run(UserServiceApp.class, args);
    }
}

