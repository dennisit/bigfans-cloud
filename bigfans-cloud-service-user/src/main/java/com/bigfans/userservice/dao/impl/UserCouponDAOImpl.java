package com.bigfans.userservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.userservice.dao.UserCouponDAO;
import com.bigfans.userservice.model.UserCoupon;
import org.springframework.stereotype.Repository;

@Repository
public class UserCouponDAOImpl extends MybatisDAOImpl<UserCoupon> implements UserCouponDAO {

    @Override
    public int use(String userId, String couponId) {
        ParameterMap params = new ParameterMap();
        params.put("userId", userId);
        params.put("couponId", couponId);
        return getSqlSession().update(className + ".use", params);
    }

    @Override
    public int getOwnedCount(String userId, String couponId) {
        ParameterMap params = new ParameterMap();
        params.put("userId", userId);
        params.put("couponId", couponId);
        Integer ownedCount = getSqlSession().selectOne(className + ".count", params);
        return ownedCount;
    }

    @Override
    public int addCount(String userId, String couponId, int count) {
        return 0;
    }
}
