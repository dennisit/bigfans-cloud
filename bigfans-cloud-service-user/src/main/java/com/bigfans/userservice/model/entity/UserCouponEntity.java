package com.bigfans.userservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Table;

@Data
@Table(name = "User_Coupon")
public class UserCouponEntity extends AbstractModel {

    private String userId;
    private String couponId;
    private Integer count;

    @Override
    public String getModule() {
        return "UserCoupon";
    }
}
