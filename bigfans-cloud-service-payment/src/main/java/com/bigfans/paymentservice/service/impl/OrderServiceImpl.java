package com.bigfans.paymentservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.paymentservice.dao.OrderDAO;
import com.bigfans.paymentservice.model.Order;
import com.bigfans.paymentservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lichong 2014年12月5日上午11:24:36
 * @Description:订单服务实现类
 */
@Service(OrderServiceImpl.BEAN_NAME)
public class OrderServiceImpl extends BaseServiceImpl<Order> implements OrderService {

    public static final String BEAN_NAME = "orderService";

    private OrderDAO orderDAO;

    @Autowired
    public OrderServiceImpl(OrderDAO orderDAO) {
        super(orderDAO);
        this.orderDAO = orderDAO;
    }

}
