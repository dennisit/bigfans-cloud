package com.bigfans.paymentservice.listener;

import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.model.event.order.OrderCreatedEvent;
import com.bigfans.paymentservice.api.clients.OrderServiceClient;
import com.bigfans.paymentservice.model.Order;
import com.bigfans.paymentservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Component
@KafkaConsumerBean
public class OrderListener {

    @Autowired
    private OrderServiceClient orderServiceClient;
    @Autowired
    private OrderService orderService;

    @KafkaListener
    public void on(OrderCreatedEvent orderCreatedEvent) {
        try {
            CompletableFuture<Order> orderFuture = orderServiceClient.getOrder(orderCreatedEvent.getOrderId());
            Order order = orderFuture.get();
            orderService.create(order);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}