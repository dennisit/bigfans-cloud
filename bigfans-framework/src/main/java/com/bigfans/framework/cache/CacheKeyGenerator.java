package com.bigfans.framework.cache;

import java.lang.reflect.Method;


/**
 * 
 * @Title: 
 * @Description: 缓存key生成接口
 * @author lichong 
 * @date 2016年1月18日 上午10:08:56 
 * @version V1.0
 */
public interface CacheKeyGenerator {

	String generate(Object target, Method method, Object... params);
	
}
