package com.bigfans.framework.dao;

import java.util.HashMap;
import java.util.Map;

public class ParameterMap extends HashMap<String, Object> {

	private static final long serialVersionUID = 4752855471677476222L;

	public ParameterMap() {
	}
	
	public ParameterMap(Map<String , Object> params) {
		this.putAll(params);
	}
	
	public ParameterMap(Long start , Long pagesize) {
		this.put(MybatisDAOImpl.START, start);
		this.put(MybatisDAOImpl.PAGESIZE, pagesize);
	}
}
