package com.bigfans.framework.es;

import java.util.Map;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月7日上午10:36:47
 *
 */
public interface DocumentConverter<T> {

	T toObject(Map<String,Object> dataMap);
	
	IndexDocument toDocument(T obj);
	
}
