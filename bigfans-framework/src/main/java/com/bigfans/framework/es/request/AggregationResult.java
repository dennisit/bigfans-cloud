package com.bigfans.framework.es.request;

import java.io.Serializable;

/**
 * 
 * @Title:
 * @Description:
 * @author lichong
 * @date 2015年10月8日 上午8:37:14
 * @version V1.0
 */
public class AggregationResult implements Serializable {

	private static final long serialVersionUID = 6811439324953197889L;
	// 用于从es返回结果中根据名字获取请求的聚合
	private String name;
	private String key;
	private long docCount;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public long getDocCount() {
		return docCount;
	}

	public void setDocCount(long docCount) {
		this.docCount = docCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AggregationResult other = (AggregationResult) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AggregationResult [name=" + name + ", key=" + key + ", docCount=" + docCount + "]";
	}

}
